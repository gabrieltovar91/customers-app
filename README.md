## Customers App
**Aplicacion de prueba hecha con React JS**

Con fines de aprendizaje, esta es la aplicacion de prueba para la gestion de clientes, generada paso a paso siguiendo el curso de React JS en Udemy.
Puedes acceder a la informacion del curso aqui: [React JS + Redux + ES6. Completo ¡De 0 a experto! (español)](https://www.udemy.com/react-js-redux-es6-completo-de-0-a-experto-espanol/)

## Comandos utiles para ejecutar

Se recomienda primero ejecutar el comando para iniciar el servidor virtual sea con NPM o con yarn

`yarn start`

Debido a que una de las dependencias para este codigo incluye el uso de JSON server (esto porque no se conecta directamente a un servidor de datos, sino que simula uno) tambien se debe ejecutar el siguiente codigo

`json-server --watch db.json --port XXXX`

**NOTA:** *se debe especificar un puerto diferente a cualquiera de los que este en uso, sin embargo, para fines de evitar la modificacion directa de los archivos del proyecto, se aconseja ejecutar el comando con el puerto 3001*