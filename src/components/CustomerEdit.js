import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { setPropsAsInitial } from '../helpers/setPropsAsInitial';
import CustomersActions from './CustomersActions';
import { Prompt } from 'react-router-dom';
import { accessControl } from '../helpers/accessControl';
import { CUSTOMER_EDIT, CUSTOMER_VIEW } from '../constans/permissions';

const isNumber = value => (
    isNaN(Number(value)) && "El campo debe ser un numero"
);

const validate = values => {
    const error = {};

    if (!values.name) {
        error.name = "El campo Nombre es requerido";
    }

    if (!values.dni) {
        error.dni = "El campo DNI es requerido";
    }

    if (values.age <= 0) {
        error.age = "La edad tiene un valor incorrecto";
    }

    return error;
};

const toNumber = value => value && Number(value);
const toUpper = value => value && value.toUpperCase();
const toLower = value => value && value.toLowerCase();
const onlyGrow = (value, previousValue) =>
                    value && (!previousValue ? value : (value > previousValue ? value : previousValue));

class CustomerEdit extends Component {

    componentDidMount() {
        if (this.txt) {
            this.txt.focus();
        }
    }

    renderField = ({withFocus, input, meta, type, label, name}) => (
        <div>
            <label htmlFor={name}>{label}</label>
            <input {...input}
                type = {!type ? "text" : type}
                ref = { withFocus && (txt => this.txt = txt) }
            />
            {
                meta.touched && meta.error && <span>{ meta.error }</span>
            }
        </div>
    );

    render() {
        const { handleSubmit, submitting, onBack, pristine, submitSucceeded} = this.props;
        return (
            <div>
                <h2>Edicion del cliente</h2>
                <form onSubmit={handleSubmit}>
                    <Field
                        withFocus
                        name = "name"
                        component = { this.renderField }
                        label = "Nombre"
                        format = {toLower}
                        parse = {toUpper}
                    />
                    <Field
                        name = "dni"
                        component = { this.renderField }
                        validate = { isNumber }
                        label = "DNI"
                    />
                    <Field
                        name = "age"
                        component = { this.renderField }
                        type = "number"
                        validate = { isNumber }
                        label = "Edad"
                        parse = {toNumber}
                        normalize = {onlyGrow}
                    />
                    <CustomersActions>
                        <button
                            type="submit"
                            disabled = { pristine || submitting }
                        >Aceptar
                        </button>
                        <button
                            type="button"
                            onClick={onBack}
                            disabled={submitting}
                        >Cancelar
                        </button>
                    </CustomersActions>
                    <Prompt
                        when = { !pristine && !submitSucceeded}
                        message = "se perderan las modificaciones. ¿Continuar?"
                    ></Prompt>
                </form>
            </div>
        );
    }
}

CustomerEdit.propTypes = {
    name: PropTypes.string,
    dni: PropTypes.string,
    age: PropTypes.number,
    onBack: PropTypes.func.isRequired,
    pristine: PropTypes.bool.isRequired,
    submitSucceeded: PropTypes.bool.isRequired,
};

export default accessControl([CUSTOMER_EDIT, CUSTOMER_VIEW])(setPropsAsInitial(reduxForm({ form: 'CustomerEdit', validate})(CustomerEdit)));