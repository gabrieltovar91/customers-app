import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import AppFrame from '../components/AppFrame';
import { getCustomerByDni } from '../selectors/customers';
import { SubmissionError } from 'redux-form';
import CustomerEdit from '../components/CustomerEdit';
import CustomerData from '../components/CustomerData';
import { fetchCustomers } from '../actions/fetchCustomers';
import { updateCustomer } from '../actions/updateCustomer';
import { deleteCustomer } from '../actions/deleteCustomer';

class CustomerContainer extends Component {

    componentDidMount() {
        if (!this.props.customer) {
            this.props.fetchCustomers();
        }
    }
    
    handleSubmit = values => {
        const { id } = values;
        return this.props.updateCustomer(id, values).then( r => {
            if (r.error) {
                throw new SubmissionError(r.payload);
            }
        }).catch(e => {
            throw new SubmissionError(e);
        });
    }

    handleOnBack = () => {
        this.props.history.goBack();
    }

    handleOnSubmitSuccess = () => {
        this.props.history.goBack();
    }

    handleOnDelete = id => {
        this.props.deleteCustomer(id).then(v => {
            this.props.history.goBack();
        });
    }

    renderCustomerControl = (isEdit, isDelete) => {
        if (this.props.customer) {
            const CustomerControl = isEdit ? CustomerEdit : CustomerData;
            return <CustomerControl {...this.props.customer}
                    onSubmit = {this.handleSubmit}
                    onSubmitSuccess = {this.handleOnSubmitSuccess}
                    onBack = {this.handleOnBack}
                    isDeleteAllow = {!!isDelete}
                    onDelete = {isDelete && this.handleOnDelete}
                    />
        }

        return null;
    }

    renderBody = () => (
        <Route
            path = "/customers/:dni/edit"
            children = {
                ( { match :isEdit } ) => (
                    <Route
                        path = "/customers/:dni/del" 
                        children = {
                        ({match : isDeleted}) => (
                            this.renderCustomerControl(isEdit, isDeleted)
                        )
                        }
                    />
                )
            }
        />
        
    )

    //<p>Datos del cliente "{this.props.customer.name}"</p>
    render() {
        return (
            <div>
                <AppFrame
                    header = { `Cliente ${this.props.dni}` }
                    body = { this.renderBody() }
                ></AppFrame>
            </div>
        );
    }
}

CustomerContainer.propTypes = {
    dni: PropTypes.string.isRequired,
    customer: PropTypes.object,
    fetchCustomers: PropTypes.func.isRequired,
    updateCustomer: PropTypes.func.isRequired,
};

const mapStateToProps = (state, props) => ({
    customer: getCustomerByDni(state, props)
});

export default withRouter(connect(mapStateToProps, {fetchCustomers, updateCustomer, deleteCustomer})(CustomerContainer));